from django.apps import AppConfig


class AmraBillingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Amra_Billing'

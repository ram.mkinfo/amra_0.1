from django.contrib import admin
from .models import Medicine,Bill,Amra_Staff
# Register your models here.
admin.site.register(Medicine)
admin.site.register(Bill)
admin.site.register(Amra_Staff)
"""Amra URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('Home', views.home, name='Home'),
    path('', views.Login, name='Login'),
    path('LoginSuccess', views.LoginSuccess, name='LoginSuccess'),
    path('Product', views.Product, name='Product'),
    path('addMedicine', views.addMedicine, name='addMedicine'),
    path('Save_Bill', views.Save_Bill, name='Save_Bill'),
    path('Sales', views.Sales, name='Sales'),
]

from django.db import models

# Create your models here.


class Medicine(models.Model):
    drug_code = models.IntegerField()
    medicine_name = models.CharField(max_length=100)
    unit = models.IntegerField()
    UNIT_SIZE = (
        ('NOS', 'Nos.'),
        ('ML', 'Milligram'),
    )
    unit_size = models.CharField(choices=UNIT_SIZE, max_length=10)
    mrp = models.FloatField()


class Bill(models.Model):
    B_item_code = models.IntegerField()
    B_Items_Name = models.CharField(max_length=100)
    B_Qty = models.IntegerField()
    B_Mrp = models.FloatField()
    B_Amount = models.FloatField()
    B_sumItems = models.IntegerField()
    B_Gross_Amount = models.FloatField()
    B_Discount = models.FloatField()
    B_Net_Amount = models.FloatField()


class Amra_Staff(models.Model):
    Name = models.CharField(max_length=100)
    Contact_Number=models.IntegerField()
    Emp_Id=models.IntegerField()
    password=models.CharField(max_length=100)

from cgitb import html
from django.contrib import messages
from django.contrib.auth.models import User, auth
from django.shortcuts import render, redirect
from .models import Amra_Staff, Medicine, Bill
# Create your views here.
from django.http import HttpResponse


def home(request):
    medicine_list = Medicine.objects.all()
    context = {'medicine_list': medicine_list}
    return render(request, 'Home.html', context)


def Login(request):
    return render(request, 'Login.html')


def LoginSuccess(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return redirect('Home')
        else:
            messages.info(request, 'Invalid Username or Password')
            return redirect('Login')
    else:
        return render(request, 'login.html')


def Product(request):
    medicine_list = Medicine.objects.all()
    context = {'medicine_list': medicine_list}
    return render(request, 'Home.html', context)


def Sales(request):
    medicine_list = Medicine.objects.all()
    context = {'medicine_list': medicine_list}
    return render(request, 'Sales.html', context)


def addMedicine(request):
    if request.method == 'POST':
        Drug_code = request.POST['Drcode']
        Mname = request.POST['Mname']
        unit = request.POST['unit']
        unitsize = request.POST['unitsize']
        Mrp = request.POST['mrp']

        MedicineData = Medicine()
        MedicineData.drug_code = Drug_code
        MedicineData.medicine_name = Mname
        MedicineData.unit = unit
        MedicineData.unit_size = unitsize
        MedicineData.mrp = Mrp
        MedicineData.save()
        messages.success(request, 'Medicine Added Successfully...')
        return redirect('Home')
    return render(request, 'Home.html')


def Save_Bill(request):
    if request.method == 'POST':
        q = request.POST['qty']
        # m_name=request.POST['mname']
        context = {
            "q": q,
            # "mname":m_name
        }
        return redirect('Home',)
        # return render(request,'Sales.html',context)
        # return redirect('Sales',q)
    # return render(request,'Home.html')
    #     BillData=Bill()
        # BillData.B_Qty=request.POST['']

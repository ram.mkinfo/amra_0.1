# Generated by Django 4.0.6 on 2022-07-25 05:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Amra_Billing', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='medicine',
            name='mrp',
            field=models.FloatField(default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='medicine',
            name='unit_size',
            field=models.CharField(choices=[('NOS', 'Nos.'), ('ML', 'Milligram')], max_length=10),
        ),
    ]

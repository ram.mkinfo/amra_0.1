# Generated by Django 4.0.6 on 2022-07-25 05:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Amra_Billing', '0003_medicine_unit'),
    ]

    operations = [
        migrations.AlterField(
            model_name='medicine',
            name='medicine_name',
            field=models.CharField(max_length=100),
        ),
    ]
